package performance

import model.{PizzaOrder, PizzaTime}
import org.scalatest.{FreeSpec, Matchers}
import services.{MinAverageCookingTimeService, PizzaCookingSimulation}

import scala.util.Random

class MinAvgTimeServicePerformanceSpec extends FreeSpec with Matchers{

  "computation should " - {

    "terminate kinda soon" in {

      val service = new MinAverageCookingTimeService

      val maxOrdersNumber = 10000
      val maxStartTime = 1000000000
      val maxCookingTime = 1000000000

      val orderIndexes = 1 to maxOrdersNumber

      val startTimes = orderIndexes.map{ _ =>
        Random.nextInt(maxStartTime)
      }.sorted

      val cookingTimes = orderIndexes.map { _ =>
        Random.nextInt(maxCookingTime)
      }

      val orders = startTimes.zip(cookingTimes).map{
        case (startTime, cookingTime) => PizzaOrder(PizzaTime(startTime), PizzaTime(cookingTime))
      }.toList

      println (service.averageCookingTimeService.averageTime(orders))
      //todo compute time spent
    }

  }



}
