package services

import model.{PizzaOrder, PizzaTime}
import org.scalatest.{FreeSpec, Matchers}

class AverageCookingTimeServiceSpec extends FreeSpec with Matchers {

  val service = new AverageCookingTimeService

  "averageTime should" - {
    AverageTimeFixture.simulations.foreach { s =>
      s.testClue in {
        service.averageTime(s.orders) shouldBe s.expectedResult
      }
    }
  }

}

object AverageTimeFixture {

  val simulations = List(
    PizzaCookingSimulation(
      Nil,
      PizzaTime(0)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(5)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        )
      ),
      PizzaTime(8)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(9)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        )
      ),
      PizzaTime(10)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(9)
        )
      ),
      PizzaTime(9)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(9)
        )
      ),
      PizzaTime(9)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(3),
          PizzaTime(9)
        )
      ),
      PizzaTime(9)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(3),
          PizzaTime(9)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(6)
        )
      ),
      PizzaTime(10)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(4),
          PizzaTime(1)
        )
      ),
      PizzaTime(1)
    )
  )

}


//case class PizzaCookingSimulation(orders: List[PizzaOrder],
//                                  expectedResult: AverageCookingTime) {
//
//  def testClue = s"return ${expectedResult.value} with following orders: ${orders.toString}"
//}