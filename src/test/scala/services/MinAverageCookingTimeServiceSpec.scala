package services

import model.{PizzaOrder, PizzaTime}
import org.scalatest.{FreeSpec, Matchers}

class MinAverageCookingTimeServiceSpec extends FreeSpec with Matchers {

  val service = new MinAverageCookingTimeService

  "minAverageTime should" - {
    PizzaCookingTimeFixture.simulations.foreach { s =>
      s.testClue in {
        runSimulation(s)
      }
    }

    "throw exception when orders are not ordered by startTimes" in {
      val simulation = PizzaCookingSimulation(
        List(
          PizzaOrder(
            PizzaTime(0),
            PizzaTime(4)
          ),
          PizzaOrder(
            PizzaTime(12),
            PizzaTime(5)
          ),
          PizzaOrder(
            PizzaTime(2),
            PizzaTime(6)
          )
        ),
        PizzaTime(5)
      )

      intercept[IllegalArgumentException] {
        service.minAverageTime(simulation.orders)
      }
    }

  }

  private def runSimulation(simulation: PizzaCookingSimulation) = {
    service.minAverageTime(simulation.orders) shouldBe simulation.expectedResult
  }

}

object PizzaCookingTimeFixture {

  val simulations = List(
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(9)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        )
      ),
      PizzaTime(9)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        )
      ),
      PizzaTime(3)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(3)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(9)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(5)
        )
      ),
      PizzaTime(8)
    ),
    PizzaCookingSimulation(
      Nil,
      PizzaTime(0)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(4),
          PizzaTime(5)
        )
      ),
      PizzaTime(7)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(2),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(5),
          PizzaTime(5)
        )
      ),
      PizzaTime(7)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(0),
          PizzaTime(4)
        ),
        PizzaOrder(
          PizzaTime(1),
          PizzaTime(6)
        ),
        PizzaOrder(
          PizzaTime(3),
          PizzaTime(9)
        )
      ),
      PizzaTime(9)
    ),
    PizzaCookingSimulation(
      List(
        PizzaOrder(
          PizzaTime(4),
          PizzaTime(1)
        )
      ),
      PizzaTime(1)
    )
  )

}


case class PizzaCookingSimulation(orders: List[PizzaOrder],
                                  expectedResult: PizzaTime) {

  def testClue = s"return ${expectedResult.value} with following orders: ${orders.toString}"
}
