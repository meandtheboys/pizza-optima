package model

case class PizzaTime(value: Long) {
  def +(other: PizzaTime) = PizzaTime(value + other.value)
  def -(other: PizzaTime) = PizzaTime(value - other.value)
  def <=(other: PizzaTime) = value <= other.value
}

object PizzaTime {
  lazy val ZERO = PizzaTime(0)
}