package model

case class PizzaOrder(startTime: PizzaTime,
                      cookingTime: PizzaTime)
