import model.{PizzaOrder, PizzaTime}
import services.MinAverageCookingTimeService

import scala.util.{Failure, Success, Try}

object Runner {

  def main(args: Array[String]) {

    val minAverageCookingTimeService = new MinAverageCookingTimeService

    println("Please enter orders quantity N:")
    val ordersQty = safelyReadPositiveIntFromConsole

    val orders = (1 to ordersQty).map { i =>
      safelyReadPizzaOrderFromConsole(i)
    }.toList

    println(s"Minimum average cooking time will be: ${minAverageCookingTimeService.minAverageTime(orders).value}")

  }

  private def safelyReadPizzaOrderFromConsole(orderIndex: Int): PizzaOrder = {
    println (s"Please enter order ${orderIndex} start and cooking times separated by space")
    var order: Try[PizzaOrder] = Failure[PizzaOrder](new Exception)
    do{
      val orderString = scala.io.StdIn.readLine
      val orderInputStrings = orderString.split(" ").toList

      order = Try{34
        PizzaOrder(
          PizzaTime(orderInputStrings.head.toLong),
          PizzaTime(orderInputStrings.tail.head.toLong)
        )
      }

      if(order.isFailure)println(s"Something went wrong, please, try to enter order ${orderIndex} again:")
    }
    while (order.isFailure)
    order.get
  }

  private def safelyReadPositiveIntFromConsole(): Int = {

    var result: Try[Int] = Failure[Int](new Exception)
    do{
      val inputLine = scala.io.StdIn.readLine()
      result = Try{
        inputLine.toInt
      }
      if (result.isFailure)
        println("Something went wrong, please, try again:")
    }
    while (result.isFailure)

    result.get
  }
}
