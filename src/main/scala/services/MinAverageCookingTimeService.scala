package services

import model._

class MinAverageCookingTimeService {

  val averageCookingTimeService = new AverageCookingTimeService

  def minAverageTime(orders: List[PizzaOrder]): PizzaTime =
    if (orders.isEmpty)
      PizzaTime.ZERO
    else
      nonEmptyOrdersMinAvgTime(orders)

  private def nonEmptyOrdersMinAvgTime(orders: List[PizzaOrder]): PizzaTime = {
    require(orders.nonEmpty)
    require(orders.sortBy(_.startTime.value) == orders)

    val (fullTime, optimalOrders, ordersLeft) =
      orders.tail.foldLeft(
        orders.head.cookingTime,
        orders.head :: Nil,
        List.empty[PizzaOrder]) {

        case ((currentOrderEndTime, optimalOrdersList, ordersQueue), order) =>
          if (order.startTime <= currentOrderEndTime)
            (currentOrderEndTime, optimalOrdersList, order :: ordersQueue)
          else {
            val sortedQueue = ordersQueue.sortBy(_.cookingTime.value)
            val nextOrder = sortedQueue.head
            (
              currentOrderEndTime + nextOrder.cookingTime,
              optimalOrdersList :+ nextOrder,
              order :: sortedQueue.tail
              )
          }
      }

    averageCookingTimeService.averageTime(optimalOrders ++ ordersLeft.sortBy(_.cookingTime.value))
  }



}
