package services

import model.{PizzaOrder, PizzaTime}

class AverageCookingTimeService {

  def averageTime(orders: List[PizzaOrder]) =
    if (orders.isEmpty)
      PizzaTime.ZERO
    else
      notEmptyOrdersAverageTime(orders)


  private def notEmptyOrdersAverageTime(orders: List[PizzaOrder]): PizzaTime = {
    require(orders.nonEmpty)
    val firstCookingStart = orders.head.startTime

    val (fullOrdersTime, waitingTimes) = orders.foldLeft((PizzaTime.ZERO, List.empty[PizzaTime])) {
      case ((timeSpentOnPreviosOrders, times), order) =>
        val waitingTime = timeSpentOnPreviosOrders + order.cookingTime - order.startTime + firstCookingStart
        (timeSpentOnPreviosOrders + order.cookingTime, waitingTime::times)
    }

    PizzaTime(
      waitingTimes.map(_.value).sum./(waitingTimes.size)
    )
  }
}
